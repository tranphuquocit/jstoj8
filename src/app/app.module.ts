import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { UpgradeModule } from '@angular/upgrade/static';
import { angular } from 'angular';




import * as app from '../app/app.module.ajs';

declare var angular: angular.IAngularStatic;

import { AppComponent } from './app.component';
import { AngularTestDirective } from './components/tests/angularjs.component';

@NgModule({
  declarations: [
    AppComponent,
    AngularTestDirective
  ],
  imports: [
    BrowserModule,
    UpgradeModule,
  ],
  providers: [
    {
      provide: '$q',
      useFactory: ($injector: any) => $injector.get('$q'),
      deps: ['$injector']
    }
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
  constructor(private upgrade: UpgradeModule) { }
  ngDoBootstrap() {
    this.upgrade.bootstrap(document.body, [app.default], { strictDi: true });
  }
}
