'use strict';

import 'angular-route';
import { angular } from 'angular';
declare var angular: angular.IAngularStatic;
import ovAlert from '../my-app/app/ov_components/ovAlert/ovAlert.js';
import ovDottie from '../my-app/app/ov_components/ovDottie/ovDottieSrv.js';
// import ovUxSelect from '../my-app/app/ov_components/ovUxSelect/ovUxSelect.js';
// import ovUtility from '../my-app/app/ov_components/ovUtility/ovUtility.js';

// angular.module('Training', [ovAlert.module, ovAlert.ovAlertBuilder, ovDottie, ovUxSelect.module, ovUtility.module])
angular.module('Training', [ovAlert.module, ovDottie.default])
.controller('ctrlQuoc', ['$scope', 'ovAlertBuilder',function ($scope, ovAlertBuilder) {
  $scope.clickAlert = function() {
    $scope.ovAlert = ovAlertBuilder.getBuilder().build();
    $scope.ovAlert._success('test');
  }
  $scope.id ='test11';
  $scope.fieldList = [
    {
      key:'test1',
      name:'test1'
    },
    {
      key:'test2',
      name:'test2'
    },
    {
      key:'test3',
      name:'test3'
    }
  ];
  $scope.ovSelectSetting = {
    search: true,
    multiple: true
  }
}]);

export default angular.module('Training');
