angular.module('Training')
.directive('angularTest', function() {
  return {
    controller: 'ctrlQuoc',
    template: `<h1>AngularJS Component</h1>
    <test-angular></test-angular>
    <button ng-click="clickAlert()">SHOW ALERT</button>
    <div ov-alert="ovAlert" alert-id="'test'"></div>
    <div class="vs-list-sort-by">
      <ov-ux-select
        ng-model="primaryKey"
        ov-select-setting = "ovSelectSetting"
        ux-select-id="{{id}}-sort-by"
        ov-ng-options="field.key as field.name for field in fieldList">
      </ov-ux-select>
    </div>
    `,
    restrict: 'E',
  }
})
