import { Directive, ElementRef, Injector } from '@angular/core';
import { UpgradeComponent } from '@angular/upgrade/static';

@Directive({
  selector: 'angular-test'
})
export class AngularTestDirective extends UpgradeComponent {
  constructor(elementRef: ElementRef, injector: Injector) {
    super('angularTest', elementRef, injector);
  }
}