import { Directive, ElementRef, Injector, SimpleChanges } from '@angular/core';
import { UpgradeComponent } from '@angular/upgrade/static';

@Directive({
  selector: 'menu-top'
})
export class MenuTopDirective extends UpgradeComponent {
  constructor(elementRef: ElementRef, injector: Injector) {
    super('menuTop', elementRef, injector);
  }
}